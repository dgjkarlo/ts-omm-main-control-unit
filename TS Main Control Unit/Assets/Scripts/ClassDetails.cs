﻿using System;

[System.Serializable]
public class ClassDetails
{
    public string ClassCode;
    public string SchoolName;
    public string ClassName;
    public string TeacherName;

    public void ClearClassDetails()
    {
        ClassCode = string.Empty;
        SchoolName = string.Empty;
        ClassName = string.Empty;
        TeacherName = string.Empty;
    }
}
