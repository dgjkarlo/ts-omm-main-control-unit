﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using UnityEngine.SceneManagement;

[Serializable]
public class StudentLoginData
{
    public string SerialCodeType;
    public string StudentName;
    public string SerialCode;
    public string ClassCode;
    public string StudentSerialCode;
}

public class GameLoader : MonoBehaviour
{
    public static GameLoader Instance;
    public TextAsset csv;

    [SerializeField] private string m_schoolName;
    [SerializeField] private string m_className;
    [SerializeField] private string m_teacherName;

   [ShowInInspector] public string[,] splitCsv;

   [ShowInInspector] private static readonly List<StudentLoginData> m_studentsData = new List<StudentLoginData>();
   private static int m_counterStudentKey;

   private void Awake()
   {
       Instance = this;
       DontDestroyOnLoad(gameObject);
   }

   private void Start()
   {
       ReadCsvFile();
   }
   
   private void ReadCsvFile()
   {
       CSVReader.DebugOutputGrid( CSVReader.SplitCsvGrid(csv.text) );
       splitCsv = CSVReader.SplitCsvGrid(csv.text);
       
       Debug.Log("Length: " + splitCsv.GetLength(1));

       for (int i = 2; i < splitCsv.GetLength(1); i++)
       {
           if (splitCsv[5, i] != null)
           {
               if (splitCsv[5, i].Equals("Student's Code") || splitCsv[5, i].Equals("Teacher's Code"))
               {
                   StudentLoginData loginData = new StudentLoginData
                   {
                       SerialCodeType = splitCsv[5, i],
                       SerialCode = splitCsv[6, i],
                       ClassCode = splitCsv[7, i],
                       StudentSerialCode = splitCsv[8, i],
                       StudentName = splitCsv[9, i],
                   };

                   m_studentsData.Add(loginData);
               }
           }
       }
       
       SceneManager.LoadScene("PlayerLoad");
   }

   public void FakeLogin()
   {
       PlayerCredentials credentials = new PlayerCredentials
       {
           playerEmail = "fake@email.com",
           playerPassword = "fake_password",
           playerUsername = "fake_username",
           playerDisplayName = "fake_name"
       };
        
       ChilliConnectController.Instance.InitializeChilliConnectAsync(credentials);
   }

    public static void Initialize()
    {
        if (m_studentsData.Count > m_counterStudentKey)
        {
            Debug.Log("Initialize ");
            
            string userName = m_studentsData[m_counterStudentKey].ClassCode.ToUpper();
            string displayName = userName;
            string password = "password_" + userName;
            string email = userName + "@email.com";
            
            PlayerCredentials credentials = new PlayerCredentials
            {
                playerEmail = email,
                playerPassword = password,
                playerUsername = userName,
                playerDisplayName = displayName
            };
            
            ChilliConnectController.Instance.InitializeChilliConnectAsync(credentials);
        }
    }
    
    private void CreateClassDetails()
    {
        ClassDetails classDetails = new ClassDetails
        {
            ClassCode = m_studentsData[m_counterStudentKey].ClassCode.ToUpper(),
            ClassName = m_className, // insert class name here
            SchoolName = m_schoolName,
            TeacherName = m_teacherName
        };

        string json = JsonUtility.ToJson(classDetails);
        
        ChilliConnectController.Instance.ChilliConnectPlayerInventory.AddPlayerInventoryWithInstanceData(
            ChilliConnectConstants.PLAYER_INVENTORY_KEY_TYPE.CLASS_DATA.ToString(),
            classDetails.ClassCode, json);
    }

    public void SetStudentDetails()
    {
        CreateClassDetails();
        
        foreach (StudentLoginData studentLoginData in m_studentsData)
        {
            List<DrillSet> sets = new List<DrillSet>();
            
            StudentData loginData = new StudentData
            {
                SerialCodeType = studentLoginData.SerialCodeType,
                StudentSerialCode = studentLoginData.StudentSerialCode,
                StudentName = studentLoginData.StudentName,
                StudentDrillSets = sets
            };
            
            string json = JsonUtility.ToJson(loginData);
            
            if (studentLoginData.SerialCodeType.Equals("Teacher's Code"))
            {
                ChilliConnectController.Instance.ChilliConnectPlayerInventory.AddPlayerInventoryWithInstanceData(
                    ChilliConnectConstants.PLAYER_INVENTORY_KEY_TYPE.TEACHER_DATA.ToString(),
                    studentLoginData.StudentSerialCode, json);
            }
            else if (studentLoginData.SerialCodeType.Equals("Student's Code"))
            {
                ChilliConnectController.Instance.ChilliConnectPlayerInventory.AddPlayerInventoryWithInstanceData(
                    ChilliConnectConstants.PLAYER_INVENTORY_KEY_TYPE.STUDENT_DATA.ToString(),
                    studentLoginData.StudentSerialCode, json);
            }
        }
    }

    
    public static void LogoutPlayer()
    {
        GameObject sdk_core = GameObject.FindGameObjectWithTag("SDK_CORE");
        
        Destroy(ChilliConnectController.Instance.gameObject);
        Destroy(sdk_core);

        m_counterStudentKey++;
        SceneManager.LoadScene("PlayerLoad");
    }
}
