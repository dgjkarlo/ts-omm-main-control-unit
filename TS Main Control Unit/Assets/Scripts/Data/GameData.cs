﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameData{
	public int level;
	public float exp;
	public int gold;
	public int score;
	public int totalScore;
	public int currentRewardPoints;
	public int totalRewardPoints;
}
