﻿using System.Collections.Generic;

[System.Serializable]
public class DrillSet
{
    public string DrillName;
    public List<DrillResult> DrillResults;
}
