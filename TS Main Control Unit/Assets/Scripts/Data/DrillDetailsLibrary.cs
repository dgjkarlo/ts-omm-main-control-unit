﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class DrillDetailsLibrary : MonoBehaviour
{
    public static DrillDetailsLibrary Instance;
    [SerializeField] private List<DrillDetails> m_drillSets;

    private void Awake()
    {
        Instance = this;
        DontDestroyOnLoad(gameObject);
    }

    public List<DrillDetails> GetDrillDetails()
    {
        return m_drillSets;
    }

    public DrillDetails GetDrillDetails(string p_drillName)
    {
        return m_drillSets.FirstOrDefault(t => t.DrillName.Equals(p_drillName));
    }
}

[System.Serializable]
public class DrillDetails
{
    public string DrillName;
    public List<string> DrillStats = new List<string>();

    public List<string> GetDrillStats()
    {
        return DrillStats;
    }
}
