﻿using UnityEngine;

namespace BOL_Base_Game_Scripts.Data
{
    [CreateAssetMenu(fileName = "New SideStory Data", menuName = "Books Of Life Scriptable/Create SideStory Data")]
    public class SideStoryData : ScriptableObject
    {
        public string SideStoryId;
        public string SideStoryName;
        public Sprite SideStoryImage;
    }
}
