﻿using System.Collections.Generic;

[System.Serializable]
public class StudentData
{
    public string SerialCodeType;
    public string StudentSerialCode;
    public string StudentName;
    public List<DrillSet> StudentDrillSets;
}
