﻿using System.Collections.Generic;

[System.Serializable]
public class PlayerBooksData
{
    public string BookId = string.Empty;
    public bool IsFavorite = false;
    public int CurrentChapterUnlocked = 0;    
    public PlayerChapterProgressData CurrentPlayerChapterProgressData;
    public BookStatus BookStatus = new BookStatus();
    public List <string> PaidButtons = new List<string>();
    public Dictionary<string, RelationshipStatus> RelationshipStatus = new Dictionary<string, RelationshipStatus>();
}

[System.Serializable]
public class BookStatus
{
    public int ConversantPortaitIndex = 1;
    public int CurrentBackgroundIndex = 0;
    public HScenePlaying HScenePlaying = new HScenePlaying();
}

[System.Serializable]
public class HScenePlaying
{
    public bool IsGalleryPlaying = false;
    public string CurrentGalleryID = string.Empty;
}

[System.Serializable]
public class RelationshipStatus
{
    public string CharacterKey = string.Empty; 
    public int RelationshipValue = 1;
}


