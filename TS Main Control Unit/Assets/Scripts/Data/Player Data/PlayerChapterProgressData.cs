﻿using System.Collections.Generic;

[System.Serializable]
public class PlayerChapterProgressData 
{
    public string ChapterId = string.Empty;
    public bool isCompleted = false;
    public bool isUnlocked = false;
    public bool isFirstChapter = false;
    public bool isBeingPlayed = false;
    public bool isReset = false;
    public List<string> ChapterKeys = new List<string>();
}
 