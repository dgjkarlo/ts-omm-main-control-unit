﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlayerSeriesData
{
    public string SeriesId;
    public Dictionary<string, PlayerBooksData> BookDetailsData = new Dictionary<string, PlayerBooksData>();
    public int CharacterChoice = 0;
    public string FirstName = "";    
    public string LastName = "";
}
