﻿public abstract class ChilliConnectConstants {
    public const string LIVE_TOKEN = "OCQxeJQFnNCraVZsxTZmYH1f6ExnzwCV";

    public enum CURRENCY_KEY_TYPE {
        CURRENCY_KEY_TYPE_LENGTH
    }
    
    public enum PLAYER_INVENTORY_KEY_TYPE
    {
        TEACHER_DATA,
        CLASS_DATA,
        STUDENT_DATA,
        PLAYER_WHITELISTING,
        PLAYER_OLD_ACCOUNT_ID,
        PLAYER_INVENTORY_DATA_TYPE_LENGTH
    }

    public enum CUSTOM_DATA_KEY_TYPE {
        CUSTOM_KEY_TYPE_LENGTH
    }

    public const string PLAYER_LAST_LOGIN_KEY = "PLAYER_LAST_LOGIN";

    public const string GET_SERVER_TIME_KEY = "GET_SERVER_TIME";

    public const string NOT_YET_RECORDED_KEY = "NOT_YET_RECORDED_KEY";
}
