﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using ChilliConnect;
using UnityEngine;
using SdkCore;

public class ChilliConnectPlayerInventory : MonoBehaviour
{
    private Action<string> m_callback;
    private bool m_doneLoading = false;

    public IEnumerator InitializeGetPlayerInventoryKeyOnly(Action<List<string>> p_callback)
    {
        GetPlayerInventoryKeyOnly(p_callback);

        while (m_doneLoading == false)
        {
            yield return null;
        }
    }

    public IEnumerator InitializeGetPlayerInventoryWithInstanceData(List<string> key, Action<Dictionary<string, MultiTypeValue>> p_callback)
    {
        GetPlayerInventoryWithInstanceData(key, p_callback);

        while (m_doneLoading == false)
        {
            yield return null;
        }
    }

    private void GetPlayerInventoryKeyOnly(Action<List<string>> p_callback)
    {
        ChilliConnectController.Instance.ChilliConnectInstance.ChilliConnect.Economy.GetInventory(

            (GetInventoryResponse response) =>
            {
                Debug.Log("<color=blue> Successfully Got Inventory: </color>" + response);

                ReadOnlyCollection<PlayerInventoryItem> playerItems = response.Items;
                List<string> inventory = new List<string>();

                if(playerItems.Count > 0)
                {
                    foreach (PlayerInventoryItem t in playerItems)
                    {
                        inventory.Add(t.Key);
                        Debug.Log("string -- " + t.ToString());
                    }
                }

                p_callback.Invoke(inventory);

                m_doneLoading = true;
            },

            (GetInventoryError error) =>
            {
                Debug.Log("<color=red> Failed to Get Inventory: </color>" + error.ErrorDescription);
                m_doneLoading = true;
            });
    }

    private void GetPlayerInventoryWithInstanceData(IList<string> key, Action<Dictionary<string, MultiTypeValue>> callback)
    {
        ChilliConnectController.Instance.ChilliConnectInstance.ChilliConnect.Economy.GetInventoryForKeys(key,

        (request, response) =>
        {
            Debug.Log("<color=blue> Successfully Got Inventory With Instance Data: </color>" + response);

            ReadOnlyCollection<PlayerInventoryItem> playerItems = response.Items;
            
            if (callback != null) 
            {
                Dictionary<string, MultiTypeValue> dict = playerItems.ToDictionary(t => t.Key, 
                    t => t.InstanceData);
                callback.Invoke(dict);
            }
            
            m_doneLoading = true;
        },

        (request, error) =>
        {
            Debug.Log("<color=red> Failed to Get Inventory With Instance Data: </color>" + error.ErrorDescription);
            m_doneLoading = true;
        });
    }

    public void AddPlayerInventoryKeyOnly(string p_key)
    {
        AddInventoryItemRequestDesc item = new AddInventoryItemRequestDesc(p_key);
        ChilliConnectController.Instance.ChilliConnectInstance.ChilliConnect.Economy.AddInventoryItem(item,

            (AddInventoryItemRequest request, AddInventoryItemResponse response) =>
            {
                Debug.Log("<color=blue> Successfully Added Item: </color>" + p_key);
            },

            (AddInventoryItemRequest request, AddInventoryItemError error) =>
            {
                Debug.Log("<color=blue> Failed to Add Item: </color>" + error.ErrorDescription);
            });
    }

    public void AddPlayerInventoryWithInstanceData(string p_key, string p_id, MultiTypeValue p_value)
    {
        AddInventoryItemRequestDesc item = new AddInventoryItemRequestDesc(p_key)
        {
            InstanceData = p_value, ItemId = p_id
        };

        ChilliConnectController.Instance.ChilliConnectInstance.ChilliConnect.Economy.AddInventoryItem(item,

            (request, response) =>
            {
                Debug.Log("<color=blue> Successfully Added Item: </color>" + p_key);
                //GameLoader.LogoutPlayer();
            },

            (request, error) =>
            {

                Debug.Log("<color=blue> Failed to Add Item: </color>" + p_id + "error: " + error.ErrorDescription);
            });
    }

    public static void UpdatePlayerInventoryWithInstanceData(string p_id, MultiTypeValue p_value)
    {
        UpdateInventoryItemRequestDesc  item = new UpdateInventoryItemRequestDesc (p_id, p_value);
        
        ChilliConnectController.Instance.ChilliConnectInstance.ChilliConnect.Economy.UpdateInventoryItem(item,

            (request, response) =>
            {
                Debug.Log("<color=blue> Successfully Update Item: </color>" + p_id);
            },

            (request, error) =>
            {
                Debug.Log("<color=blue> Failed to Update Item: </color>" + p_id + "error: " + error.ErrorDescription);
            });
    }

    public void RemovePlayerInventoryWithInstanceData(string p_id)
    {
        RemoveInventoryItemRequestDesc  item = new RemoveInventoryItemRequestDesc (p_id);
        
        ChilliConnectController.Instance.ChilliConnectInstance.ChilliConnect.Economy.RemoveInventoryItem(item,

            request =>
            {
                Debug.Log("<color=blue> Successfully Remove Item: </color>" + p_id);
            },

            (request, error) =>
            {
                Debug.Log("<color=blue> Failed to Remove Item: </color>" + error.ErrorDescription);
            });
    }
}
