﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using ChilliConnect;
using UnityEngine;

public class ChilliConnectCustomDefinition : MonoBehaviour {

    private bool m_doneLoading = false;

    /// <summary>
    /// Initialize Get Player Currency Data.
    /// It is in a Coroutine. To make sure that it will finish getting the data before doing another task.
    /// It requires a key that you set in the Chilli Connect in a string form data type. 
    /// You can also set a certain callback that it will do after you get the data you need.
    /// </summary>
    public IEnumerator InitializeGetCustomData (string customType, Action<IDictionary<string, object>, string> callback = null) {
        GetCustomData (customType, callback);

        while (m_doneLoading == false) {
            yield return null;
        }
    }

    /// <summary>
    /// Get the Player Cuurency Data.
    /// It requires a key that you set in the Chilli Connect in a string form data type. 
    /// You can also set a certain callback that it will do after you get the data you need.
    /// </summary>
    public void GetCustomData (string type, Action<IDictionary<string, object>, string> callback = null) {

        m_doneLoading = false;

        GetCustomDefinitionsRequestDesc customDesc = new GetCustomDefinitionsRequestDesc (type);
        customDesc.ItemKey = type;
        customDesc.TypeKey = "Book";

        ChilliConnectController.Instance.ChilliConnectInstance.ChilliConnect.Catalog.GetCustomDefinitions (customDesc,

            (GetCustomDefinitionsRequest request, GetCustomDefinitionsResponse response) => {
                Debug.Log ("<color=blue> Successfully Got Custom Definition Data: </color>" + response);

                ReadOnlyCollection<CustomDefinition> items = response.Items;

                Debug.Log ("Countooo: " + items.Count);

                for (int i = 0; i < items.Count; i++) {
                    callback.Invoke (items [i].Data.AsDictionary ( ).Serialise ( ), type + "_" + i);
                }

                m_doneLoading = true;
            },

            (GetCustomDefinitionsRequest request, GetCustomDefinitionsError error) => {
                Debug.Log ("<color=red> Failed to Get Custom Definition Data: </color>" + error.ErrorDescription);
                m_doneLoading = true;
            });
    }

    /// <summary>
    /// Set the Player Currency Data.
    /// It requires a key that you set in the Chilli Connect in a string form data type. 
    /// It also requries the value that you need to set in that certain key in a string form data type.
    /// </summary>
    public void SetCurrency (string currencyType, int value) {
        SetCurrencyBalanceRequestDesc currencyDesc = new SetCurrencyBalanceRequestDesc (currencyType.ToString ( ), value);

        ChilliConnectController.Instance.ChilliConnectInstance.ChilliConnect.Economy.SetCurrencyBalance (currencyDesc,
            (SetCurrencyBalanceRequest request, SetCurrencyBalanceResponse response) => {
                Debug.Log ("<color=blue> Successfully Set Currency: </color>" + response);
            },

            (SetCurrencyBalanceRequest request, SetCurrencyBalanceError error) => {
                Debug.Log ("<color=red> Failed to Set Currency: </color>" + error.ErrorDescription);
            });
    }

}
