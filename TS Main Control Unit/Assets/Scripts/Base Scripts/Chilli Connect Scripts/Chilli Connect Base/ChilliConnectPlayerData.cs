﻿using System;
using System.Collections;
using System.Collections.Generic;
using ChilliConnect;
using SdkCore;
using UnityEngine;

public class ChilliConnectPlayerData : MonoBehaviour {

    private Action<string> m_callback;
    private bool m_doneLoading = false;

    /// <summary>
    /// Initialize Get Player Data.
    /// It is in a Coroutine. To make sure that it will finish getting the data before doing another task.
    /// It requires a key that you set in the Chilli Connect in a string form data type. 
    /// You can also set a certain callback that it will do after you get the data you need.
    /// </summary>
    public IEnumerator InitializeGetPlayerData (string playerDataType, Action<MultiTypeValue> p_callback = null) {
        GetPlayerData (playerDataType, p_callback);

        while (m_doneLoading == false) {
            yield return null;
        }
    }

    /// <summary>
    /// Get the Player Data.
    /// It requires a key that you set in the Chilli Connect in a string form data type. 
    /// You can also set a certain callback that it will do after you get the data you need.
    /// </summary>
    public void GetPlayerData (string playerDataType, Action<MultiTypeValue> callback = null) {
        m_doneLoading = false;

        var keys = new List<string> ( );
        keys.Add (playerDataType);

        ChilliConnectController.Instance.ChilliConnectInstance.ChilliConnect.CloudData.GetPlayerData (keys,
            (GetPlayerDataRequest request, GetPlayerDataResponse response) => {
                Debug.Log ("<color=blue> Successfully Get Player Data: </color>" + response);

                if (callback != null && response.Values.Count > 0) {
                    if (response.Values [0].Value != null) {
                        callback.Invoke (response.Values [0].Value);
                    } else {
                        Debug.Log ("<color=red> The record is Null or Empty for this key: </color>" + playerDataType);
                    }
                } else {
                    callback.Invoke (ChilliConnectConstants.NOT_YET_RECORDED_KEY);
                    Debug.Log ("<color=red> There is no key recorded for this key: </color>" + playerDataType);
                }

                m_doneLoading = true;
            },
            (GetPlayerDataRequest request, GetPlayerDataError error) => {
                Debug.Log ("<color=red> Failed to Get Player Data: </color>" + error.ErrorDescription);
                m_doneLoading = true;
            });
    }

    /// <summary>
    /// Set the Player Data.
    /// It requires a key that you set in the Chilli Connect in a string form data type. 
    /// It also requries the value that you need to set in that certain key in a string form data type.
    /// </summary>
    public void SetPlayerData (string playerDataType, MultiTypeValue value) {
        SetPlayerDataRequestDesc desc = new SetPlayerDataRequestDesc (playerDataType, value);

        ChilliConnectController.Instance.ChilliConnectInstance.ChilliConnect.CloudData.SetPlayerData (desc,
            (SetPlayerDataRequest request, SetPlayerDataResponse response) => {
                Debug.Log ("<color=blue> Successfully Set Player Data: </color>" + playerDataType);
            },
            (SetPlayerDataRequest request, SetPlayerDataError error) => {
                Debug.Log ("<color=red> Failed to Set Player Data: </color>" + error.ErrorDescription);
            });
    }
}
