﻿using System;
using UnityEngine;

public class ChilliConnectInit : MonoBehaviour {

    /// <summary>
    /// Initialize the Chilli Connect SDK
    /// You need the token that you will get in the ChilliConnect website after you created a game data.
    /// </summary>
    public void InitChilliConnectSDK (Action p_callback) {
        InitSDK (p_callback);
    }

    /// <summary>
    /// Initialize the Chilli Connect SDK
    /// It requires a token in a string data form.
    /// </summary>
    private void InitSDK (Action p_callback) {
        Debug.Log ("<color=yellow> Initializing Chilli Connect SDK... </color>");

        if (ChilliConnectController.Instance.ChilliConnectInstance.ChilliConnect == null) {
            ChilliConnectController.Instance.ChilliConnectInstance.ChilliConnect = new ChilliConnect.ChilliConnectSdk (ChilliConnectConstants.LIVE_TOKEN, true);

            if (p_callback != null) {
                p_callback.Invoke ( );
            }
        }
    }
}
