﻿using UnityEngine;

public class ChilliConnectCredential : MonoBehaviour
{
    public string ChilliConnectUsername;
    public string ChilliConnectPassword;
}
