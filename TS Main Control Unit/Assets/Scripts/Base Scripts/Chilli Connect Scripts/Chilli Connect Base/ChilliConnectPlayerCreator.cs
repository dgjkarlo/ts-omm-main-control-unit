﻿using System;
using ChilliConnect;
using UnityEngine;

[Serializable]
public class PlayerCredentials
{
    public string playerUsername = "";
    public string playerPassword = "password";
    public string playerDisplayName = "";
    public string playerEmail = "nutaku@nutaku.com";
}

public class ChilliConnectPlayerCreator : MonoBehaviour
{

    /// <summary>
    /// Init Chilli Connect Player Creator and Login the Player Via Username 
    /// </summary>
    public void InitChilliConnectPlayerCreator(PlayerCredentials p_credential, Action p_callback = null)
    {
        Debug.Log("<color=yellow> Initializing Player Creator... </color>");
        PlayerLoggingInViaUsername(p_credential, p_callback);
    }

    /// <summary>
    /// Create an anonymous player and register that player in Chilli Connect Server.
    /// </summary>
    private void CreatePlayer(PlayerCredentials p_credential, Action p_callback = null)
    {
        Debug.Log("<color=yellow> Creating New Player... </color>");

        ChilliConnectController.Instance.m_isFirstTimePlayer = true;
        //Save nutaku accounts and register
        CreatePlayerRequestDesc createPlayerRequestDesc = new CreatePlayerRequestDesc
        {
            Email = p_credential.playerEmail,
            UserName = p_credential.playerUsername,
            DisplayName = p_credential.playerDisplayName,
            Password = p_credential.playerPassword
        };
        //better if save here is the nutaku accound ID

        ChilliConnectController.Instance.ChilliConnectInstance.ChilliConnect.PlayerAccounts.CreatePlayer(createPlayerRequestDesc,
            (CreatePlayerRequest request, CreatePlayerResponse response) =>
            {
                // ChilliConnectController.Instance.ChilliConnectCredential.ChilliConnectId = response.ChilliConnectId;
                // ChilliConnectController.Instance.ChilliConnectCredential.ChilliConnectSecret = response.ChilliConnectSecret;

                Debug.Log("<color=blue> Player created with ChilliConnectId: </color>" + response.ChilliConnectId);
                Debug.Log("<color=blue> Player created with ChilliConnectSecret: </color>" + response.ChilliConnectSecret);

                // if (p_callback != null)
                // {
                //     p_callback.Invoke();
                // }

                PlayerLoggingInViaUsername(p_credential, p_callback);
            },
            (CreatePlayerRequest request, CreatePlayerError error) =>
            {
                Debug.Log("<color=red> An error occurred while creating a new player: </color>" + error.ErrorDescription);
            });
    }

    /// <summary>
    /// Login the player Via Username and if it failed, create a player credential.
    /// </summary>
    public void PlayerLoggingInViaUsername(PlayerCredentials p_credential, Action p_callback = null)
    {
        Debug.Log("<color=yellow> Logging In Player Via Username... </color>");

        LogInUsingUserNameRequestDesc loginDesc = new LogInUsingUserNameRequestDesc(p_credential.playerUsername, p_credential.playerPassword);
        
        ChilliConnectController.Instance.ChilliConnectInstance.ChilliConnect.PlayerAccounts.LogInUsingUserName(loginDesc,
            (LogInUsingUserNameRequest request, LogInUsingUserNameResponse response) =>
            {
                Debug.Log("<color=blue> Player logged in successfully via username with ChilliConnectId: </color>" + response.ChilliConnectId);

                if (p_callback != null)
                {
                    ChilliConnectController.Instance.ChilliConnectID = response.ChilliConnectId;
                    p_callback.Invoke();
                }
            },
            (LogInUsingUserNameRequest request, LogInUsingUserNameError error) =>
            {
                Debug.Log("<color=red> Failed to Login Error Code: </color>" + error.ErrorDescription);

                if (error.ErrorCode.Equals(LogInUsingUserNameError.Error.InvalidCredentials))
                {
                    CreatePlayer(p_credential, p_callback);
                }
                else
                {
                    Debug.Log("<color=red> An error occurred while logging in player via username: </color>" + error.ErrorCode);
                }
            });
    }

    public void UpdatePlayerDetails(PlayerCredentials p_credential, Action p_callback = null)
    {
        SetPlayerDetailsRequestDesc updatePlayerRequest = new SetPlayerDetailsRequestDesc
        {
            UserName = "OLD_" + p_credential.playerUsername,
            Email = "OLD_" + p_credential.playerEmail,
            DisplayName = "OLD_" + p_credential.playerDisplayName
        };

        ChilliConnectController.Instance.ChilliConnectInstance.ChilliConnect.PlayerAccounts.SetPlayerDetails(updatePlayerRequest,
            (request,  response) =>
            {
                Debug.Log("<color=blue> Player updated player data successfully via username. </color>");
                
                if (p_callback != null)
                {
                    //ChilliConnectController.Instance.ChilliConnectID = response.ChilliConnectId;
                   // p_callback.Invoke();
                }
            },
            (request, error) =>
            {
                Debug.Log("<color=red> Failed to update the player with Error Code: </color>" + error.ErrorDescription);
            });
    }
}
