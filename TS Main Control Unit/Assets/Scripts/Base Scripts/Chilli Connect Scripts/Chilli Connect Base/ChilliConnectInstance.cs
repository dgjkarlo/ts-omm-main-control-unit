﻿using ChilliConnect;
using UnityEngine;


public class ChilliConnectInstance : MonoBehaviour
{
    /// <summary>
    /// Variable that holds the Chilli Connect SDK.
    /// </summary>
    public ChilliConnectSdk ChilliConnect;
}
