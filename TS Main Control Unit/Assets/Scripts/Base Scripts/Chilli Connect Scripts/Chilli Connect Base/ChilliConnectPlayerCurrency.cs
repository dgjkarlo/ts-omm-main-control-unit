﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using ChilliConnect;
using UnityEngine;

public class ChilliConnectPlayerCurrency : MonoBehaviour {

    private bool m_doneLoading = false;
        
    /// <summary>
    /// Initialize Get Player Currency Data.
    /// It is in a Coroutine. To make sure that it will finish getting the data before doing another task.
    /// It requires a key that you set in the Chilli Connect in a string form data type. 
    /// You can also set a certain callback that it will do after you get the data you need.
    /// </summary>
    public IEnumerator InitializeGetCurrency (string currencyType, Action<int> callback = null) {
        GetCurrencyData (currencyType, callback);

        while (m_doneLoading == false) {
            yield return null;
        }
    }

    /// <summary>
    /// Get the Player Cuurency Data.
    /// It requires a key that you set in the Chilli Connect in a string form data type. 
    /// You can also set a certain callback that it will do after you get the data you need.
    /// </summary>
    public void GetCurrencyData (string currencyType, Action<int> callback = null) {

        m_doneLoading = false;

        GetCurrencyBalanceRequestDesc currencyDesc = new GetCurrencyBalanceRequestDesc ( );

        var type = new List<string> ( );
        type.Add (currencyType);
        currencyDesc.Keys = type;

        ChilliConnectController.Instance.ChilliConnectInstance.ChilliConnect.Economy.GetCurrencyBalance (currencyDesc,
            (GetCurrencyBalanceRequest request, GetCurrencyBalanceResponse response) => {
                Debug.Log ("<color=blue> Successfully Got Currency: </color>" + response);

                ReadOnlyCollection<CurrencyBalance> currencyData = response.Balances;

                if (callback != null && currencyData.Count > 0) {
                    if (String.IsNullOrEmpty (currencyData [0].Balance.ToString ( )) == false) {
                        callback.Invoke (currencyData [0].Balance);
                    } else {
                        Debug.Log ("<color=red> The record is Null or Empty for this key: </color>" + currencyType);
                    }
                } else {
                    Debug.Log ("<color=red> There is no key recorded for this key: </color>" + currencyType);
                }

                m_doneLoading = true;
            },
            (GetCurrencyBalanceRequest request, GetCurrencyBalanceError error) => {
                Debug.Log ("<color=red> Failed to Get Currency: </color>" + error.ErrorDescription);
                m_doneLoading = true;
            });
    }

    /// <summary>
    /// Set the Player Currency Data.
    /// It requires a key that you set in the Chilli Connect in a string form data type. 
    /// It also requries the value that you need to set in that certain key in a string form data type.
    /// </summary>
    public void SetCurrency (string currencyType, int value) {
        SetCurrencyBalanceRequestDesc currencyDesc = new SetCurrencyBalanceRequestDesc (currencyType.ToString ( ), value);

        ChilliConnectController.Instance.ChilliConnectInstance.ChilliConnect.Economy.SetCurrencyBalance (currencyDesc,
            (SetCurrencyBalanceRequest request, SetCurrencyBalanceResponse response) => {
                Debug.Log ("<color=blue> Successfully Set Currency: </color>" + response);
            },

            (SetCurrencyBalanceRequest request, SetCurrencyBalanceError error) => {
                Debug.Log ("<color=red> Failed to Set Currency: </color>" + error.ErrorDescription);
            });
    }

}
