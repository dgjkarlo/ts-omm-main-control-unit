﻿using UnityEngine;
using ChilliConnect;
using System;
using System.Collections;

public class ChilliConnectCloudCode : MonoBehaviour
{
    private bool m_doneLoading = false;

    public IEnumerator InitializeGetCloudCode (string key, Action<string> p_callback = null) {
        GetCloudCode (key, p_callback);

        while (m_doneLoading == false) {
            yield return null;
        }
    }

    public void GetCloudCode(string key, Action<string> callback)
    {
        RunScriptRequestDesc runScript = new RunScriptRequestDesc(key);

        ChilliConnectController.Instance.ChilliConnectInstance.ChilliConnect.CloudCode.RunScript(runScript, 
         
        (RunScriptRequest request, RunScriptResponse response) => {
                Debug.Log ("<color=blue> Successfully Got Cloud Code: </color>" + response.Output.AsDictionary()["CurrentTime"].AsString());
                string server_time = response.Output.AsDictionary()["CurrentTime"].AsString();
                callback.Invoke(server_time);
                
                m_doneLoading = true;
            },
            (RunScriptRequest request, RunScriptError error) => {
                Debug.Log ("<color=red> Failed to Get Cloud Code </color>" + error.ErrorDescription);

                m_doneLoading = true;
            });
    }
}
