﻿using System;
using System.Collections;
using System.Collections.Generic;
using SdkCore;
using Sirenix.OdinInspector;
using UnityEngine;
// ReSharper disable Unity.PerformanceCriticalCodeInvocation

public class ChilliConnectController : MonoBehaviour
{

    /// <summary>
    /// Initialize the Chilli Connect Singleton
    /// </summary>
    public static ChilliConnectController Instance;

    /// <summary>
    /// Initialize the Chilli Connect Bases
    /// </summary>
    [SerializeField] private ChilliConnectInit m_chilliConnectInit;

    private ChilliConnectInit ChilliConnectInit => m_chilliConnectInit;

    /// <summary>
    /// Initialize the Chilli Connect Base Variables
    /// </summary>
    [SerializeField] private ChilliConnectInstance m_chilliConnectInstance;
    public ChilliConnectInstance ChilliConnectInstance => m_chilliConnectInstance;

    [SerializeField] private ChilliConnectCloudCode m_chilliConnectCloudCode;
    private ChilliConnectCloudCode ChilliConnectCloudCode => m_chilliConnectCloudCode;

    [SerializeField] private ChilliConnectPlayerData m_chilliConnectPlayerData;
    public ChilliConnectPlayerData ChilliConnectPlayerData => m_chilliConnectPlayerData;

    [SerializeField] private ChilliConnectPlayerCreator m_chilliConnectPlayerCreator;
    public ChilliConnectPlayerCreator ChilliConnectPlayerCreator => m_chilliConnectPlayerCreator;

    [SerializeField] private ChilliConnectPlayerCurrency m_chilliConnectPlayerCurrency;
    private ChilliConnectPlayerCurrency ChilliConnectPlayerCurrency => m_chilliConnectPlayerCurrency;
    
    [SerializeField] private ChilliConnectPlayerInventory m_chilliConnectPlayerInventory;
    public ChilliConnectPlayerInventory ChilliConnectPlayerInventory => m_chilliConnectPlayerInventory;

    [SerializeField] private ChilliConnectCustomDefinition m_chilliConnectCustomDefinition;
    private ChilliConnectCustomDefinition ChilliConnectCustomDefinition => m_chilliConnectCustomDefinition;

    [field: ShowInInspector]
    public Dictionary<ChilliConnectConstants.CURRENCY_KEY_TYPE, int> PlayerCurrency { get; set; } = new Dictionary<ChilliConnectConstants.CURRENCY_KEY_TYPE, int>();
    
    [ShowInInspector] private Dictionary<string, IDictionary<string, object>> m_customDefinitions = new Dictionary<string, IDictionary<string, object>>();

    [ShowInInspector] private DateTime m_playerLastLogin;

    private DateTime PlayerLastLogin
    {
        get => m_playerLastLogin;
        set
        {
            m_playerLastLogin = value;
            ChilliConnectPlayerData.SetPlayerData(ChilliConnectConstants.PLAYER_LAST_LOGIN_KEY, m_playerLastLogin.ToString("yyyy-M-d h:m:s"));
        }
    }

    [SerializeField] private PlayerCredentials m_playerCredential;
    public PlayerCredentials PlayerCredentials => m_playerCredential;

    public string ChilliConnectID;

    public bool m_isFirstTimePlayer = false;


    [ShowInInspector] private DateTime m_serverTime;

    //For the Meantime
    private void Awake()
    {
        DontDestroyOnLoad(transform.gameObject);
        Instance = this;

        // InitializeChilliConnectAsync();c
    }

    /// <summary>
    /// Start the Initialization of Chilli Connect.
    /// It requires a callback. So after a task, it will call the callback to continue the initialization.
    /// </summary>
    public void InitializeChilliConnectAsync(PlayerCredentials p_playerCredentials)
    {
        m_playerCredential = p_playerCredentials;
        ChilliConnectInit.InitChilliConnectSDK(InitializePlayerCreation); //Initialize Chilli Connect SDK
    }

    /// <summary>
    /// Start the Initialization of Player Creation.
    /// It requires a callback. So after a task, it will call the callback to continue the initialization.
    /// It also requries the player credential that will be check if the player data exists. 
    /// If not, it will generate a new player data that will be sent to Chilli Connect.
    /// </summary>
    private void InitializePlayerCreation()
    {
        ChilliConnectPlayerCreator.InitChilliConnectPlayerCreator(m_playerCredential, SetupPlayerDataInitialization); //Initalize Player Creation


    }

    /// <summary>
    /// Start the Initialization of Player Data (Player Currency and Player Data).
    /// It is in a Coroutine. To make sure that it will finish getting the data before doing another task.
    /// </summary>
    private static void SetupPlayerDataInitialization()
    {
       GameLoader.Instance.SetStudentDetails();
        
        //StartCoroutine(InitializePlayerDetails());
    }

    //Test Set Currency
    public void SetCurrency()
    {
        m_chilliConnectPlayerCurrency.SetCurrency("KEYS", 100);
        m_chilliConnectPlayerCurrency.SetCurrency("DIAMOND", 200);
    }

    //Test Set Player Data
    public void SetPlayerData()
    {
        m_chilliConnectPlayerData.SetPlayerData(ChilliConnectConstants.PLAYER_LAST_LOGIN_KEY.ToString(), DateTime.Now.ToString("yyyy-M-d h:m:s"));
    }

    /// <summary>
    /// Start the Initialization of Player Details.
    /// It is in a Coroutine. To make sure that it will finish getting the data before doing another task.
    /// It requires Callbacks. So after a task, it will call the Callbacks to continue the initialization.
    /// </summary>
    private IEnumerator InitializeServerTime()
    {
        UnityEngine.Debug.Log("<color=yellow> Getting Server Time... </color>");
        yield return StartCoroutine (ChilliConnectCloudCode.InitializeGetCloudCode(ChilliConnectConstants.GET_SERVER_TIME_KEY, (string p_value) =>
        {
            DateTime server_date = DateTime.Parse(p_value);
            Debug.Log("<color=yellow> Done getting server time - </color>" + server_date);
            m_serverTime = server_date;
        }));
    }

    public DateTime GetServerTime()
    {
       return m_serverTime;
    }

    private IEnumerator InitializePlayerDetails()
    {
        Debug.Log("<color=yellow> Initializing Player Details and Getting Server Time... </color>");
        
        yield return StartCoroutine(InitializePlayerCurrency());
        if (!m_isFirstTimePlayer)
        {
            yield return StartCoroutine(InitializePlayerInventoryWithInstanceData());
        }
        else
        {
           // PlayerDataManager.Instance.InitPlayerData();
            //ChilliConnectMultiplayer.Instance.Initialise();
        }
    }

    /// <summary>
    /// Start the Initialization of Player Currency.
    /// It is in a Coroutine. To make sure that it will finish getting the data before doing another task.
    /// It requires Currency Key Type that you set in the Chilli Connect.
    /// It requires Dictionary with a string and int to hold the data.
    /// </summary>
    private IEnumerator InitializePlayerCurrency()
    {
        Debug.Log("<color=yellow> Initializing Player Currency... </color>");

        for (int i = 0; i < (int)ChilliConnectConstants.CURRENCY_KEY_TYPE.CURRENCY_KEY_TYPE_LENGTH; i++)
        {
            string currencyType = ((ChilliConnectConstants.CURRENCY_KEY_TYPE)i).ToString();

            int iChecker = i;
            yield return StartCoroutine(ChilliConnectPlayerCurrency.InitializeGetCurrency(currencyType, (p_value) =>
            {
                PlayerCurrency.Add((ChilliConnectConstants.CURRENCY_KEY_TYPE)iChecker, p_value);
            }));
        }
    }

    /// <summary>
    /// Start the Initialization of Player Last Login.
    /// It is in a Coroutine. To make sure that it will finish getting the data before doing another task.
    /// It requires Currency Key Type that you set in the Chilli Connect.
    /// It requires Dictionary with a string and int to hold the data.
    /// </summary>
    // private IEnumerator InitializePlayerLastLogin()
    // {
        // Debug.Log("<color=yellow> Initializing Player Last Login... </color>");
        // yield return StartCoroutine(ChilliConnectPlayerData.InitializeGetPlayerData(ChilliConnectConstants.PLAYER_LAST_LOGIN_KEY.ToString(),
        //     (MultiTypeValue p_value) =>
        //     {
        //         DateTime lastLogin;

        //         if (p_value.Equals(ChilliConnectConstants.NOT_YET_RECORDED_KEY))
        //         {
        //             PlayerLastLogin = DateTime.Now;
        //             Debug.Log("<color=blue> Player Last Login Created: </color>" + DateTime.Now);
        //         }
        //         else
        //         {
        //             if (DateTime.TryParse(p_value.AsString(), out lastLogin))
        //             {
        //                 lastLogin = Convert.ToDateTime(p_value);
        //                 m_playerLastLogin = lastLogin;
        //                 Debug.Log("<color=blue> Player Last Login: </color>" + m_playerLastLogin);
        //             }
        //             else
        //             {
        //                 Debug.Log("<color=red> The Player Last Login In data is not in DateTime Format: </color>" + m_playerLastLogin);
        //             }
        //         }
        //     }));
    // }
    
    private IEnumerator InitializePlayerInventoryWithInstanceData()
    {
        List<string> keys = new List<string>();
        bool isDone = false;

        for(int i=0; i<(int)ChilliConnectConstants.PLAYER_INVENTORY_KEY_TYPE.PLAYER_INVENTORY_DATA_TYPE_LENGTH; i++)   
        {
            string dataType = ((ChilliConnectConstants.PLAYER_INVENTORY_KEY_TYPE)i).ToString();
            keys.Add(dataType);
    
            int modulo = (i % 5);

            if (!modulo.Equals(0) &&
                i != ((int) ChilliConnectConstants.PLAYER_INVENTORY_KEY_TYPE.PLAYER_INVENTORY_DATA_TYPE_LENGTH) -
                1) continue;

            int iChecker = i;
            yield return StartCoroutine(ChilliConnectPlayerInventory.
                InitializeGetPlayerInventoryWithInstanceData(keys, p_value =>
            {
                foreach (KeyValuePair<string, MultiTypeValue> item in p_value)
                {
                    //PlayerDataManager.Instance.PlayerData.Add(item.Key, item.Value);
                }
                
                if(iChecker == ((int)ChilliConnectConstants.PLAYER_INVENTORY_KEY_TYPE.
                    PLAYER_INVENTORY_DATA_TYPE_LENGTH) - 1)
                {
                    isDone = true;
                }
                else
                {
                    keys.Clear();
                }
            }));
        }
    
        while(!isDone)
        {
            yield return null;
        }
        
       // PlayerDataManager.Instance.InitPlayerData();
        //ChilliConnectMultiplayer.Instance.Initialise();
    }


    private IEnumerator InitializeCustomDefinitions()
    {
        Debug.Log("<color=yellow> Initializing Player Data... </color>");

        for (int i = 0; i < (int)ChilliConnectConstants.CUSTOM_DATA_KEY_TYPE.CUSTOM_KEY_TYPE_LENGTH; i++)
        {
            string dataType = ((ChilliConnectConstants.CUSTOM_DATA_KEY_TYPE)i).ToString();

            yield return StartCoroutine(ChilliConnectCustomDefinition.InitializeGetCustomData(dataType, (IDictionary<string, object> p_value, string p_key) =>
            {
                m_customDefinitions.Add(p_key, p_value);
            }));

        }
    }
}
