﻿
[System.Serializable]
public class CardDetails
{
    public string CardKey = string.Empty;
    public int ServantId = 0;
    public int ServantQuantity = 0;
    public int Up = 0;
    public int Down = 0;
    public int Right= 0;
    public int Left = 0 ;

    public string GetCardId()
    {
        return  "servant_" + ServantId;
    }
}
