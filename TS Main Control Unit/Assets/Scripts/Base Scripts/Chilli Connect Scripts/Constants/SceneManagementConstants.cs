﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class SceneManagementConstants
{
    public const string Init_ChilliConnect_Scene = "ChilliConnectInit";
    public const string PlayerCreation_ChilliConnect_Scene = "ChilliConnectPlayerCreation";

}
